import javax.swing.*;

public abstract class Subject extends JComponent{
    public abstract void register(Listener listener);
    public abstract void deregister();
    public abstract void notify_listener();
}
